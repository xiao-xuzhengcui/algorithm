/**
 * 匹配符
 * 给你一个字符串s和一个字符规律p，请你来实现一个支持 '.'和'*'的正则表达式匹配。
 *
 * '.' 匹配任意单个字符
 * '*' 匹配零个或多个前面的那一个元素
 * 所谓匹配，是要涵盖整个字符串s的，而不是部分字符串。
 *
 *
 *  动态规划太难了
 */
public class Q10 {
    public static void main(String[] args) {

    }
    public boolean isMatch(String s, String p) {
        if(!p.contains("*")&&!p.contains(".")){
            if(s.equals(p)){
                return true;
            }else{
                return false;
            }
        }

        return false;
    }
}
