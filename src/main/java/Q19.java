import linked.ListNode;

/**
 * 删除链表的倒数第N个节点，并且返回链表的头节点
 *
 *
 * 解法：先计算链表长度，再遍历
 */
public class Q19 {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode1 = new ListNode(2);
        ListNode listNode2 = new ListNode(3);
        listNode.next=listNode1;
        listNode1.next=listNode2;
        Q19 q19 = new Q19();
        q19.removeNthFromEnd(listNode,2);
    }

    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode currentNode = head;
        Integer length = 0;
        while (currentNode !=null){
            currentNode = currentNode.next;
            length++;
        }
        if(n>length){
            return null;
        }
        currentNode = head;
        ListNode tempNode = null;
        for (int i = 0; i < length; i++) {
            if((length - i) == n){
                if(i == 0){
                    return currentNode.next;
                }
                if(i==length-1){
                    tempNode.next = null;
                    return head;
                }
                tempNode.next = currentNode.next;
                return head;
            }
            tempNode = currentNode;
            currentNode = currentNode.next;
        }
        return null;
    }
}
