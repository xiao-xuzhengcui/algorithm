import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 有效括号
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。
 *
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 *
 * 解法：栈
 */
public class Q20 {
    public static void main(String[] args) {
        Q20 q20 = new Q20();
        q20.isValid("()");
    }
    public boolean isValid(String s) {
        Stack<String> stringStack =new Stack<>();
        Map<String,String> map = new HashMap<>();
        map.put("(",")");
        map.put("[","]");
        map.put("{","}");
        for (int i = 0; i <s.length(); i++) {
            String currentStr = String.valueOf(s.charAt(i));
            if(")".equals(currentStr)||"]".equals(currentStr)||"}".equals(currentStr)){
                if(stringStack.size() == 0){
                    return false;
                }
                String pop = stringStack.pop();
                if(!pop.equals(currentStr)){
                    return false;
                }
            }
            if("(".equals(currentStr)||"[".equals(currentStr)||"{".equals(currentStr)){
                stringStack.push(map.get(currentStr));
            }
        }
        if(stringStack.size()!=0){
            return false;
        }
        return true;
    }

}
