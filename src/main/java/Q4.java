import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Q4 {

    public static void main(String[] args) {
        int[] a = new int[2];
        int[] b = new int[2];
        a[0] = 1;
        a[1] = 2;
        b[3] = 2;
        b[4] = 2;
        Q4 q4 = new Q4();
        q4.findMedianSortedArrays(a,b);

    }

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        ArrayList<Integer> integers = new ArrayList<Integer>();
        for (int i = 0; i < nums1.length; i++) {
            integers.add(nums1[i]);
        }
        for (int i = 0; i < nums2.length; i++) {
            integers.add(nums2[i]);
        }
        List<Integer> collect = integers.stream().sorted().collect(Collectors.toList());
        if(collect.size()==1){
            return collect.get(0);
        }
        int isdouble =  collect.size()%2;
        int tempindex = collect.size()/2;
        if(isdouble==0){
            return (collect.get(tempindex)+collect.get(tempindex-1))/2;
        }else{
            return collect.get(tempindex);
        }
    }
}
