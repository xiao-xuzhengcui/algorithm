/**
 * z字形转换
 *
 * 将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
 *
 * 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下：
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 *
 */
public class Q6 {
    public static void main(String[] args) {
        int temp = 5%2;
        System.out.println(temp);
    }
    public String convert(String s, int numRows) {
        if(numRows == 1){
            return s;
        }
        StringBuffer[] stringBuffers = new StringBuffer[numRows];
        for (int i = 0; i < stringBuffers.length; i++) {
            stringBuffers[i] =new StringBuffer();
        }

        for (int i = 0; i < s.length(); i++) {
            int i1 = i % (numRows + numRows - 2);
            if(i1<numRows){
                stringBuffers[i1].append(String.valueOf(s.charAt(i)));
            }else{
                stringBuffers[numRows-1-(i1-(numRows-1))].append( String.valueOf(s.charAt(i)));
            }
        }
        StringBuffer finalStringBuffer = new StringBuffer();
        for (int i = 0; i < stringBuffers.length; i++) {
            finalStringBuffer.append(stringBuffers[i]);
        }
        return finalStringBuffer.toString();
    }
}
