import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * 查找一个字符串中不重复的最长字符串
 */
public class Q3Three {
    public static void main(String[] args) {
        Q3Three q3Three = new Q3Three();
        int a = q3Three.lengthOfLongestSubstring("abcabcbb");
        String tempa = new String("abc");
        int c = tempa.charAt(0);
        System.out.println(c);
        System.out.println(a);
    }
    public int lengthOfLongestSubstring(String s) {
        if(s==null || s.equals("")){
            return 0;
        }
        if(s.length()==1){
            return 1;
        }
        int maxLength = 1;
        HashSet<String> stringHashSet = new HashSet<String>();
        int ri = 0;
        for (int k = 0; k < s.length(); k++) {
            if(k!=0){
                stringHashSet.remove(String.valueOf(s.charAt(k-1)));
            }
            while (ri < s.length() && !stringHashSet.contains(String.valueOf(s.charAt(ri)))){
                stringHashSet.add(String.valueOf(s.charAt(ri)));
                ri+=1;
            }
            Integer tempLength = ri - k;
            if(tempLength >maxLength){
                maxLength = tempLength;
            }
        }
        return maxLength;
    }
}
