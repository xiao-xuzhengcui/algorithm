/**
 * 回文数字，
 * 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。
 *
 * 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 *
 * 例如，121 是回文，而 123 不是。
 *
 * 办法：反转一半
 *
 */
public class Q9 {

    public static void main(String[] args) {
        Q9 q9 = new Q9();
        boolean palindrome = q9.isPalindrome(88888);
        System.out.println(palindrome);
    }

    public boolean isPalindrome(int x) {
        if(x < 0 || (x%10 == 0 && x>9)){
            return false;
        }
        if(x>=0 && x<=9){
            return true;
        }
        int reInt = x;
        int newInt = 0;
        int iint = 0;
        while (reInt > newInt){
            int tempint = reInt%10;
            reInt = reInt/10;
            newInt = newInt*10+tempint;
            iint+=1;
        }

        if(newInt == reInt){
            return true;
        }
        if(reInt == (newInt/10)){
            return true;
        }
        return false;
    }
}
