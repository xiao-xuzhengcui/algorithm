import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 三数之和
 * 给你一个包含 n 个整数的数组nums，判断nums中是否存在三个元素 a，b，c ，使得a + b + c = 0 ？请你找出所有和为 0 且不重复的三元组。
 *
 * 注意：答案中不可以包含重复的三元组。
 *
 *
 * 特判，对于数组长度 nn，如果数组为 nullnull 或者数组长度小于 33，返回 [][]。
 * 对数组进行排序。
 * 遍历排序后数组：
 * 若 nums[i]>0nums[i]>0：因为已经排序好，所以后面不可能有三个数加和等于 00，直接返回结果。
 * 对于重复元素：跳过，避免出现重复解
 * 令左指针 L=i+1L=i+1，右指针 R=n-1R=n−1，当 L<RL<R 时，执行循环：
 * 当 nums[i]+nums[L]+nums[R]==0nums[i]+nums[L]+nums[R]==0，执行循环，判断左界和右界是否和下一位置重复，去除重复解。并同时将 L,RL,R 移到下一位置，寻找新的解
 * 若和大于 00，说明 nums[R]nums[R] 太大，RR 左移
 * 若和小于 00，说明 nums[L]nums[L] 太小，LL 右移
 */
public class Q15 {
    public static void main(String[] args) {
        Q15 q15 = new Q15();
        int integers[] = {-1,0,1,2,-1,-4};

        q15.threeSum(integers);
    }
    public List<List<Integer>> threeSum(int[] nums) {
        if(nums.length<3){
            return new ArrayList<>();
        }
        List<List<Integer>> integers = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if(nums[i]>0){
                break;
            }
            if(i!=0 && nums[i] == nums[i-1]){
                continue;
            }
            int startindex = i+1;
            int endIndex = nums.length-1;
            while (startindex<endIndex){
                if(startindex != i+1 && nums[startindex] == nums[startindex-1]){
                    startindex++;
                    continue;
                }
                if(endIndex != nums.length-1 && nums[endIndex] == nums[endIndex+1]){
                    endIndex--;
                    continue;
                }
                Integer temp = nums[i]+nums[startindex]+nums[endIndex];
                if(temp == 0){
                    ArrayList<Integer> integersTemp = new ArrayList<>();
                    integersTemp.add(nums[i]);
                    integersTemp.add(nums[startindex]);
                    integersTemp.add(nums[endIndex]);
                    integers.add(integersTemp);
                    startindex++;
                }
                if(temp<0){
                    startindex++;
                }else{
                    endIndex--;
                }
            }
        }
        return integers;
    }
}
