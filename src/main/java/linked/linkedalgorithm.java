package linked;

/**
 * @author xiaoli
 * 链表相关算法
 */
public class linkedalgorithm {
    /**
     *反转链表
     * 描述：给定一个单链表的头结点pHead(该头节点是有值的，比如在下图，它的val是1)，长度为n，反转该链表后，返回新链表的表头。
     * 输入：{1,2,3}
     * 输出：{3,2,1}
     * @param head
     * @return
     */
     public ListNode ReverseList(ListNode head){
         ListNode cur = head;
         ListNode pre = null;
         while (cur !=null){
             ListNode temp = cur.next;
             cur.next = pre;
             pre = cur;
             cur = temp;
         }
        return pre;
     }

    /**
     * 指定区间内反转
     * 将一个节点数为 size 链表 m 位置到 n 位置之间的区间反转，要求时间复杂度 O(n)O(n)，空间复杂度 O(1)O(1)。
     * 输入：{1,2,3,4,5},2,4
     * 返回值：{1,4,3,2,5}
     * @return
     */
    public ListNode ReverseRanageList(ListNode head,int start,int end){
        ListNode cur = head;
        ListNode pre = null;
        int a = 0;
        while (cur!=null){
            a++;
            if(a>start &&a<end){

            }
        }
        return null;
    }



    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode1 = new ListNode(2);
        ListNode listNode2 = new ListNode(3);
        ListNode listNode3 = new ListNode(4);
        listNode2.next = listNode3;
        listNode1.next = listNode2;
        listNode.next = listNode1;
        linkedalgorithm linkedalgorithm = new linkedalgorithm();
        ListNode listNode4 = linkedalgorithm.ReverseList(listNode);
        System.out.println(listNode4);
    }



}
