import java.util.Arrays;

/**
 * 给定一个数组和一个目标数，获取相加的和与目标数最接近的三个数
 */
public class Q16 {
    public static void main(String[] args) {

    }
    public int threeSumClosest(int[] nums, int target) {
        if(nums.length<3){
            return 0;
        }
        Arrays.sort(nums);
        int resultInt = nums[nums.length-1]+nums[nums.length-2]+nums[nums.length-3];
        for (int i = 0; i < nums.length; i++) {
            int startIndex = i+1;
            int endIndex = nums.length-1;
            while (startIndex <endIndex){
                int tempResultInt = nums[i]+nums[startIndex]+nums[endIndex];
                if(Math.abs(tempResultInt-target)<Math.abs(resultInt-target)){
                    resultInt = tempResultInt;
                }
                if(tempResultInt == target){
                    return resultInt;
                }
                if(tempResultInt > target){
                    endIndex--;
                }
                if(tempResultInt < target){
                    startIndex++;
                }
            }
        }
        return resultInt;
    }
}
