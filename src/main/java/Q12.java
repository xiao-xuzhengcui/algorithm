import java.util.HashMap;
import java.util.Map;

/**
 * 罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
 * 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。
 * 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。
 * 同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况
 * I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
 * X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。
 * C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
 * 给你一个整数，将其转为罗马数字。
 * 输入: num = 3
 * 输出: "III"
 *
 * 提示 1 <= num <= 3999
 *
 */
public class Q12 {
    public static void main(String[] args) {
        Q12 q12 = new Q12();
        q12.intToRoman(58);
    }
    public String intToRoman(int num) {
        StringBuffer stringBuffer = new StringBuffer();
        int thround = num/1000;
        if((thround) > 0 ){
            for (int i = 0; i <thround; i++) {
                stringBuffer.append("M");
            }
            num = num % 1000;
        }
        int hundred = num/100;
        if((hundred) > 0 ){
            if(hundred==4){
                stringBuffer.append("CD");
            }else if(hundred==9){
                stringBuffer.append("CM");
            }else{
             if(hundred<5){
                 for (int i = 0; i < hundred; i++) {
                     stringBuffer.append("C");
                 }
             }
             else{
                 stringBuffer.append("D");
                 for (int i = 0; i < hundred - 5; i++) {
                     stringBuffer.append("C");
                 }
             }
            }
            num = num % 100;
        }
        int ten = num/10;
        if((ten) > 0 ){
            if(ten==4){
                stringBuffer.append("XL");
            }else if(ten==9){
                stringBuffer.append("XC");
            }else{
                if(ten<5){
                    for (int i = 0; i < ten; i++) {
                        stringBuffer.append("X");
                    }
                }
                else{
                    stringBuffer.append("L");
                    for (int i = 0; i < ten - 5; i++) {
                        stringBuffer.append("X");
                    }
                }
            }
            num = num % 10;
        }

        if((num) > 0 ){
            if(num==4){
                stringBuffer.append("IV");
            }else if(num==9){
                stringBuffer.append("IX");
            }else{
                if(num<5){
                    for (int i = 0; i < num; i++) {
                        stringBuffer.append("I");
                    }
                }
                else{
                    stringBuffer.append("V");
                    for (int i = 0; i < num - 5; i++) {
                        stringBuffer.append("I");
                    }
                }
            }
        }



        return stringBuffer.toString();
    }
}
