import java.util.Stack;

/**
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 *
 * 如果反转后整数超过 32 位的有符号整数的范围[−2的31次方, 2的31次方− 1] ，就返回 0。
 *
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 *
 */
public class Q7 {

    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE);
        Q7 q7 = new Q7();
        int reverse = q7.reverse(1534236469);
        System.out.println(reverse);
    }

    public int reverse(int x) {
        String s = String.valueOf(x);
        boolean isInt = s.contains("-");

        StringBuffer str = new StringBuffer();
        if(isInt){
            s = s.substring(1,s.length());
            str.append("-");
        }

        Stack<String> stringStack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            stringStack.push(String.valueOf(s.charAt(i)));
        }
        while (!stringStack.empty()){
            String pop = stringStack.pop();
            str.append(pop);
        }
        Integer reverseInt = 0;
        try{
            reverseInt = Integer.parseInt(str.toString());
        }catch (Exception e){

        }

        if(isInt && reverseInt < Math.pow(-2,31)){
            return 0;
        }
        if(!isInt && reverseInt >Math.pow(2,31)-1){
            return 0;
        }
        return  reverseInt;
    }
}
