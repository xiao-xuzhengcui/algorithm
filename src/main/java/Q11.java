

/**
 * 给定一个长度为 n 的整数数组height。有n条垂线，第 i 条线的两个端点是(i, 0)和(i, height[i])。
 *
 * 找出其中的两条线，使得它们与x轴共同构成的容器可以容纳最多的水。
 *
 * 返回容器可以储存的最大水量。
 *
 * 例如：
 * 输入：[1,8,6,2,5,4,8,3,7]
 * 输出：49
 *
 *解法：双指针
 *
 */
public class Q11 {

    public static void main(String[] args) {

    }

    public int maxArea(int[] height) {
        if(height.length<=1){
            return 0;
        }
        int startIndex = 0;
        int endIndex = height.length-1;
        int width = height.length-1;
        int max = 0;
//        int finalStartIndex = 0;
//        int finalEndIndex = height.length-1;
        while (endIndex>startIndex){
            int temp = 0;
            if(height[startIndex]> height[endIndex]){
                temp = width*height[endIndex];
            }else{
                temp = width*height[startIndex];
            }

            if(temp >max){
//                finalStartIndex = startIndex;
//                finalEndIndex = endIndex;
                max = temp;
            }

            if(height[startIndex]> height[endIndex]){
                endIndex--;
            }else{
                startIndex++;
            }
            width--;
        }
        return max;
    }
}
