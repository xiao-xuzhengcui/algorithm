import java.util.HashMap;
import java.util.Map;

/**
 * 问题1 ，两数之和
 * 给定一个数组和目标数，获取数组中两个数相加等于目标数的数字
 */
public class Q1one {
    public static void main(String[] args) {

    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length ; i++) {
            if(map.containsKey(target-nums[i])){
                int[] ints = new int[2];
                ints[0] = i;
                ints[1] = map.get(target-nums[i]);
                return ints;
            }
            map.put(nums[i],i);
        }
        throw new RuntimeException("错误信息");
    }
}
