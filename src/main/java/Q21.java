import linked.ListNode;

import java.util.List;

/**
 * 合并有序列表
 *
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 *
 * 可以用递归，或者迭代
 */
public class Q21 {
    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(4);
        listNode1.next =listNode2;
        listNode2.next =listNode3;

        ListNode listNode4 = new ListNode(1);
        ListNode listNode5 = new ListNode(3);
        ListNode listNode6 = new ListNode(4);
        listNode4.next =listNode5;
        listNode5.next =listNode6;

        Q21 q21 = new Q21();
        q21.mergeTwoLists(listNode1,listNode4);


    }
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode headNode = null;
        ListNode currentNode = headNode;
        if(list1==null){
            return list2;
        }
        if(list2==null){
            return list1;
        }
        ListNode currentNode1 = list1;
        ListNode currentNode2 = list2;
        while (currentNode1 != null){
            while (currentNode2!=null && currentNode1.val >= currentNode2.val ){
                if(headNode == null){
                    headNode = new ListNode(currentNode2.val) ;
                    currentNode = headNode;
                }else{
                    currentNode.next = currentNode2;
                    currentNode = currentNode.next;
                }
                currentNode2 = currentNode2.next;
            }
            if(headNode == null){
                headNode = new ListNode(currentNode1.val) ;
                currentNode = headNode;
            }else{
                currentNode.next = currentNode1;
                currentNode = currentNode.next;
            }
            currentNode1 = currentNode1.next;
        }
        if(currentNode1 == null && currentNode2 != null){
            while (currentNode2!=null){
                currentNode.next = currentNode2;
                currentNode = currentNode.next;
                currentNode2 = currentNode2.next;
            }
        }
        return headNode;
    }
}
